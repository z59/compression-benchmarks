#!/bin/sh

_lzop() {
	time lzop -9 "$RF" >"$CF"
	time lzop -d <"$CF" >/dev/null
}
_zstd() {
	time zstd -c --single-thread --ultra -22 "$RF" >"$CF"
	time zstd -d --single-thread <"$CF" >/dev/null
}
_xz() {
	time xz -c -z -9 "$RF" >"$CF"
	time xz -d <"$CF" >/dev/null
}

RF=core22-509.tar
CF=$RF.x

RS=$(wc -c <"$RF")
echo "$RF raw size: $RS bytes"
shuf -e lzop zstd xz | while IFS= read -r L; do
	rm -f "$CF"
	"$L" -V
	"_$L"
	CS=$(wc -c <"$CF")
	echo "Compressed size: $CS bytes. Ratio: $(echo " $CS/$RS" | bc -ql)"
done
